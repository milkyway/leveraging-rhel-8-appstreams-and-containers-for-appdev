#!/bin/bash
podman run -it --rm \
    -v "$PWD":/wd:z \
    --workdir /wd \
    core.example.com:5000/appstream-lab/fedpkg-container "$@"
