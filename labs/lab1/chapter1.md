[//]: # (Comment: This document uses the practice of breaking all sentences in to new lines to ease diff comparisons.)
# Lab 1: Introduction to Application Streams
## Introduction

In this lab we will introduce the concepts and basic commands to using Applications Streams in RHEL8.
Specifically, we will cover:
  * Review of how to use Application Streams
  * Review of when to use Application Streams
  * Review of relationship between Application Streams & Modules
  * Review of what Application Streams are available from Red Hat & Elsewhere

This lab should be performed on `workstation.example.com` as `student` unless otherwise instructed.

Expected completion: 15-20 minutes

NOTE: much of the superflous output from the commands run here has been redacted for brevity. Please don't consider it an error.

## Prerequisites

This lab requires that you have completed [Lab 0](../lab0/chapter0.md).

If you have not done so, or have been unable to complete it successfully, there is an ansible playbook on `workstation.example.com` that will set the system up.

```
$ cd ~/appstream-lab/playbooks
$ ansible-playbook -i hosts --tags=lab1 lab-setup-play.yml
```
This should only take a moment to complete.

## yum4 & new repos

With RHEL8, you may have noticed some new repositories have become available (and some have gone away).

RHEL7:
* Server : main repository of content
* Optional : extra content that should be used primarily for dependencies
* Supplemental : license restricted content
* Extras : fast-changing content to be used in limited scenarios

RHEL8:
* BaseOS : the software you need to run a "userspace" on physical, virtual or containerized platforms
* Application Stream (AppStream) : the software that you deploy in a "userspace"
* Code Ready Linux Builder (CRB) : the software you need to build against for some scenarios
* Supplemental : license restricted content

Let's check them out:

```bash
$ sudo yum repolist
```

You will find the short names of the repos and a brief description. If you want more information, it would be worth checking out some blog posts:
* [RHEL8 Application Streams](https://developers.redhat.com/blog/2018/11/15/rhel8-introducing-appstreams/)
* [CodeReady Linux Builder](https://developers.redhat.com/blog/2018/11/15/introducing-codeready-linux-builder/)

Hopefully, that makes sense.
Let's dig in to Application Streams in more detail.

## Getting software

```bash
$ sudo yum -y install postgresql
$ rpm -ql postgresql | less
```

What happened?

You got psql, the client tool for the postgresql database.
You also got a bunch of docs and helper tools.
Hmmm.
Isn't that the same as RHEL7 except a newer version?

Yep.

So, how is this new?

Well, let's try:

```bash
$ sudo yum module list postgresql
Name        Stream     Profiles            Summary
postgresql  10 [d][e]  client, server [d]  PostgreSQL server and client module
postgresql  9.6        client, server [d]  PostgreSQL server and client module
```

Well, now you see from the second column that there are two "streams" available.
So what is a "stream"?

A "stream" is short for "Application Stream."
In the case of postgresql it is pretty simple.
We have two streams, and each stream provides a different version of postgresql.
Both streams are available and supported.

How does that work? Well, in RHEL8 we have a new feature that allows us to turn on and off parts of a repository which we think of as "virtual repositories." We do that by adding "module metadata" (or `modulemd` for short) to the repo-data.

Let's take a quick look at the modulemd for postgresql.

```bash
$ curl http://core.example.com/repos/rhel8/ga/AppStream/repodata/87ada5e5d9c759dccdff8955fc93c33760454907021411ef552d3a6a8ca5ecc5-modules.yaml.gz |gunzip |less
```

Inside you will find the module definitions.
While in this `less` session, search for "name: postgresql" by hitting "/" and then type `name: postgresql` in `less`. You will find the modulemd for one of the postgresql streams.
Search again (by pressing "n" in `less`) and you should find the modulemd for the other postgresql stream.

It is beyond the scope of this lab to review the modulemd specification in detail.
However, if you are interested in learning more, the specification is very well documented [upstream](https://github.com/fedora-modularity/libmodulemd/blob/master/spec.v2.yaml).

As you just saw, there are two of these yaml documents for postgresql in the AppStream repodata -- 
one for "`stream: 10`" and one for "`stream: 9.6`".

But, when you typed `sudo yum -y install postgresql` it just worked.
How does the system know which stream you should get? This is where the concept of "defaults" comes in.

Before we go on, however, we need to reset a couple things.

```bash
$ sudo yum -y remove postgresql
$ sudo yum -y module reset postgresql
```

## Defaults

In order to simplify the usage of RHEL8, modules can have a "default" stream.
The default stream is defined by the maintainer(s) to be the preferred stream if the end user has no opinion.
Let's take a look at how "default" is defined.

```bash
$ curl http://core.example.com/repos/rhel8/ga/AppStream/repodata/87ada5e5d9c759dccdff8955fc93c33760454907021411ef552d3a6a8ca5ecc5-modules.yaml.gz |gunzip |less
```

If you search for "document: modulemd-defaults" (e.g. hit "/" then type `document: modulemd-defaults` in `less`) you will find the beginning of the list of stream defaults.
If you then search for "module: postgresql" you will find the stream defaults for postgresql.

Output:

```yaml
---
document: modulemd-defaults
version: 1
data:
  module: postgresql
  stream: 10
  profiles:
    10: [server]
    9.6: [server]
...
```

This block of yaml describes the "module defaults" for the postgresql module.
This line `stream: 10` indicates to yum that the stream called "10" should be considered the default when a user types something like `yum install postgresql`.

We can see the defaults by checking for "`[d]`" next to the "`10`" under "`Stream`".

```bash
$ sudo yum module list postgresql
Name        Stream  Profiles            Summary
postgresql  10 [d]  client, server [d]  PostgreSQL server and client module
postgresql  9.6     client, server [d]  PostgreSQL server and client module
```

You may have noticed something else in the defaults file.
Specifically, a section called `profiles`.
First off, what is a `profile`? Well, a `profile` defines a set of content (packages) to install for a given use case.

For example, back in the modulemd file, the postgresql module defined:

```yaml
  profiles:
    client:
      rpms:
      - postgresql
    server:
      rpms:
      - postgresql-server
```

That means that there are two profiles.
First, there is the `client` profile which installs one rpm (postgresql) when activated.
Second, there is the `server` profile which also installs one rpm, but in this case `postgresql-server`.

In order to mimic the experience of RHEL7, a default profile is also chosen, per stream, to replicate the expectation established in prior versions of RHEL.
In the case of `postgresql`, it is to install the server by default.
Interestingly, in the case of mysql/mariadb it is the opposite. When you install `mariadb` you get the client by default.

So, why did you get the client when you ran `yum install postgresql`
before? When using `yum` commands that operate on RPMs (the same
way it worked in RHEL 7), you get only RPMs. In this case, you told `yum` to
install the `postgresql` RPM, so it retrieved that from the
`postgresql:10` stream and installed it for you. If you wanted the
module default profile, you should call
`yum module install postgresql` instead.

## Streams and Profiles
For future reference, you can easily examine the profiles available for an Application Stream with the `--profile` flag, which lists the profiles and the rpms they will install. Give it a try:

```bash
$ sudo yum -C module info postgresql --profile
Name   : postgresql:10:820190104140132:9edba152:x86_64
client : postgresql
server : postgresql-server

Name   : postgresql:9.6:820190104140337:9edba152:x86_64
client : postgresql
server : postgresql-server
```

Let's try using different streams and profiles.
We could install the postgresql v10 server by just executing `sudo yum install @postgresql` (but we aren't going to do that right now so we don't have to clean up).
As you can see, the syntax for installing the default stream and profile is the same as if this was a yum group.
However, we can change streams and which profile using a slight variation. Please execute:

```bash
$ sudo yum -y install @postgresql:9.6/client
```

This command will install the `client` profile from the `9.6 stream`.
We provided `/client` to enable the `client` profile which only installs the postgresql client.
Note that if we had left `/client` off we would have gotten the default profile, `server`, indicated by the `[d]` next to `server` below.

```bash
$ sudo yum module list postgresql
Name        Stream   Profiles                Summary
postgresql  10 [d]   client, server [d]      PostgreSQL server and client module
postgresql  9.6 [e]  client [i], server [d]  PostgreSQL server and client module
```

Now, here is what is interesting.
```bash
$ sudo yum list postgresql
Installed Packages
postgresql.x86_64                           9.6.10<details>
```

Yet, we know there is a postgresql v10 in there somewhere. So, let's experiment with that.

```bash
$ sudo yum -y module disable postgresql
<snip />
Complete!
$ sudo yum -y module enable postgresql:10
<snip />
Complete!
$ sudo yum list postgresql
Installed Packages
postgresql.x86_64       9.6.10
Available Packages
postgresql.x86_64       10.6-1
```

Nice.
We have an upgrade available that was not there when we were on the 9.6 stream.
Now let's upgrade that.

```bash
$ sudo yum -y update postgresql
<snip />
Complete!
$ sudo yum list postgresql
Installed Packages
postgresql.x86_64       10.6
```

Be aware that under normal circumstances, RHEL8 will never force you to update
to an incompatible package (`postgresql:10` is not
backwards-compatible with `postgresql:9.6`). This example is
intended to demonstrate how you could make a conscious choice to switch to
another version. <em>It is not expected that this will upgrade existing
database instances cleanly</em>!

Now let's install both the client and server profiles, because we want both on this computer.

```bash
$ sudo yum install -y @postgresql/{server,client}
<snip />
Complete!
$ sudo yum list postgresql-server
Installed Packages
postgresql-server.x86_64       10.6
```

OK, now let's move on to the [next lab](../lab2/chapter2.md) where we can start making our own module!
