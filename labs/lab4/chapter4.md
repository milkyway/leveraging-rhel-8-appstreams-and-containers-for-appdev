[//]: # (Comment: This document uses the practice of breaking all sentences in to new lines to ease diff comparisons.)
# Lab 4: Using Profiles & Defaults
## Introduction

In this lab we will convert our application to use Application Stream Profiles also known as Module Profiles. We will also see how changing the configured Application Stream Defaults can also make our software simpler.

This lab should be performed on `workstation.example.com` as `student` unless otherwise instructed.

Expected completion: 15-20 minutes

NOTE: much of the superflous output from the commands run here has been redacted for brevity. Please don't consider it an error.

## Prerequisites
This lab requires that you have completed [Lab 3](../lab3/chapter3.md).

If you have not done so (or have been unable to complete it successfully, there is an ansible playbook on `workstation.example.com` that will set the system up.

```bash
$ cd ~/appstream-lab/playbooks
$ ansible-playbook -i hosts --tags=lab4 lab-setup-play.yml
```
This may take up to 20 minutes to complete, depending on the current system state.

## Making a New Profile

In lab 3, you may have noticed it was a little cumbersome to setup and install the dependencies for our flask app.
What if we could make that easier?
Well, we discussed `profiles` way back in lab1, those may be able to help.
What if we made a `blog-app profile` and then used that to get the dependencies we need?

Let's start with the Python 2 version.
First, we need to check our dependencies, so let's take a look at the relevant part of our `OCIfile` from lab 3.

```docker
RUN yum \
    install -y python2 python2-flask \
    python2-flask-sqlalchemy python2-psycopg2 \
    && yum clean all
```

As you can see, we need `python2`, `python2` `python2-flask` `python2-flask-sqlalchemy` `python2-psycopg2`.
So, let's add them to a new profile, but not in the flask module, let's create a new one just for this application.

```bash
$ mkdir -p ~/flasklab/blog-app-deps
$ cd ~/flasklab/blog-app-deps
$ git init
$ git commit --allow-empty -m "Initial creation of repository for blog-app-deps module"
$ git checkout -b py27 master
$ cat <<EOF > blog-app-deps.yaml
---
document: modulemd
version: 2
data:
  name: blog-app-deps
  stream: py27
  summary: Flask demo
  description: >-
    Flask demo. Python 2 version.
  license:
    module:
    - MIT
  dependencies:
  - buildrequires:
      platform: [el8]
      python27: [2.7]
      flask: [py27]
    requires:
      platform: [el8]
      python27: [2.7]
      flask: [py27]
  profiles:
    default:
      description: Dependencies required for the blog app
      rpms:
      - python2
      - python2-flask
      - python2-flask-sqlalchemy
      - python2-psycopg2
...
EOF
$ git add blog-app-deps.yaml
$ git commit -m "blog-app-deps with a profile"
```

Now, let's build the module.

```bash
$ cd ~/flasklab/
$ sudo /home/student/bin/mbs-manager build_module_locally --offline --file blog-app-deps/blog-app-deps.yaml --stream py27 -s platform:el8 -p platform:el8 -r /etc/yum.repos.d/rhel8.repo | tee build-blog-27.log
$ sudo rm -rf /home/student/flasklab/repos/blog-app-deps-py27
$ sudo mv modulebuild/builds/$(ls -t1 modulebuild/builds |head -n1)/results /home/student/flasklab/repos/blog-app-deps-py27
```

Let's do the same for the Python 3 version.

```bash
$ cd ~/flasklab/blog-app-deps
$ git checkout -b py36 master
$ cat <<EOF > blog-app-deps.yaml
---
document: modulemd
version: 2
data:
  name: blog-app-deps
  stream: py36
  summary: Flask demo
  description: >-
    Flask demo. Python 3 version.
  license:
    module:
    - MIT
  dependencies:
  - buildrequires:
      platform: [el8]
      python36: [3.6]
      flask: [py36]
    requires:
      platform: [el8]
      python36: [3.6]
      flask: [py36]
  profiles:
    default:
      description: Dependencies required for the blog app
      rpms:
      - python36
      - python3-flask
      - python3-flask-sqlalchemy
      - python3-psycopg2
...
EOF
$ git add blog-app-deps.yaml
$ git commit -m "blog-app-deps with a profile"
$ cd ~/flasklab/
$ sudo /home/student/bin/mbs-manager build_module_locally --offline --file blog-app-deps/blog-app-deps.yaml --stream py36 -s platform:el8 -p platform:el8 -r /etc/yum.repos.d/rhel8.repo | tee build-blog-36.log
$ sudo rm -rf /home/student/flasklab/repos/blog-app-deps-py36
$ sudo mv modulebuild/builds/$(ls -t1 modulebuild/builds |head -n1)/results /home/student/flasklab/repos/blog-app-deps-py36
```

Now we should have new Python 2 & Python 3 modules built.

## Using the New Profiles

Next let's improve our frontend application by simplifying our `OCIfiles` to just use the `profiles` we built.

```bash
$ cd ~/flasklab/frontend
$ cp ~/appstream-lab/labs/lab4/app/local.repo .
```

```docker
$ cat <<EOF > flask27.OCIfile
FROM flask-base

ADD ../repos/blog-app-deps-py27 /opt/blog-app-deps-py27
ADD local.repo /etc/yum.repos.d/local.repo
RUN yum \
    module -y install blog-app-deps:py27/default \
    && yum clean all

#copy our app in to the container
ADD app.py /app/app.py
ADD templates /app/templates
EXPOSE 5000

CMD [ "/usr/bin/python2", "/app/app.py" ]

EOF
```

And now let's do the Python 3 version.

```docker
$ cat <<EOF > flask36.OCIfile
FROM flask-base

ADD ../repos/blog-app-deps-py36 /opt/blog-app-deps-py36
ADD local.repo /etc/yum.repos.d/local.repo
RUN yum \
    module -y install blog-app-deps:py36/default \
    && yum clean all

#copy our app in to the container
ADD app.py /app/app.py
ADD templates /app/templates
EXPOSE 5000

CMD [ "/usr/bin/python3", "/app/app.py" ]

EOF
```

Let's go ahead and build them both.
Then we can try and make sure they work.

```bash
$ sudo podman build -t flask27 --file=flask27.OCIfile .
$ sudo podman run --rm \
    -p 5000:5000 \
    -e DBUSER="user" \
    -e DBPASS="pass" \
    -e DBHOST="workstation.example.com" \
    -e DBNAME="db" \
    flask27
```

Ok, see if it is running at something like `http://appstream-2f1c.rhpds.opentlc.com:5000`.
Hit `Ctrl-C` when you are done making sure it works.
Now let's do the same thing for the Python 3 version.

```bash
$ sudo podman build -t flask36 --file=flask36.OCIfile .
$ sudo podman run --rm \
    -p 5000:5000 \
    -e DBUSER="user" \
    -e DBPASS="pass" \
    -e DBHOST="workstation.example.com" \
    -e DBNAME="db" \
    flask36
```

Test it again at `http://appstream-2f1c.rhpds.opentlc.com:5000`.
Hit `Ctrl-C` when you are done making sure it works.

Certainly simpler.
However, those two `OCIfiles` are starting to look remarkably similar.
Is there anything we can do to make it even simpler?

## Enter Defaults

Well, a `default stream` and a `default profile` might do the trick.
Let's go ahead and make them.

```yaml
$ cd ~/flasklab/frontend
$ cat <<EOF > blog-app-deps-python2.defaults
---
document: modulemd-defaults
version: 1
data:
  module: blog-app-deps
  stream: py27
  profiles:
    py27: [ default ]
...
EOF
```
and

```yaml
$ cat <<EOF > blog-app-deps-python3.defaults
---
document: modulemd-defaults
version: 1
data:
  module: blog-app-deps
  stream: py36
  profiles:
    py36: [ default ]
...
EOF
```

What if we inserted one of those right before the install command?
Then we could have an `OCIfile` that looks like this:

```docker
$ cat <<EOF > OCIfile
FROM flask-base
ARG PYTHON

#these could be added to our base container QED
ADD ../repos/blog-app-deps-py27 /opt/blog-app-deps-py27
ADD ../repos/blog-app-deps-py36 /opt/blog-app-deps-py36
ADD local.repo /etc/yum.repos.d/local.repo

ADD blog-app-deps-\${PYTHON}.defaults /etc/dnf/modules.defaults.d/blog-app-deps-defaults.yaml
RUN yum \
    install -y @blog-app-deps \
    && yum clean all

#copy our app in to the container
ADD app.py /app/app.py
ADD templates /app/templates
EXPOSE 5000

ENV python "/usr/bin/\${PYTHON}"
CMD \$python /app/app.py

EOF
```

OK, so let's build them, providing the python version as a build argument.

```bash
$ sudo podman build -t flask27 --no-cache --build-arg PYTHON=python2 --file=OCIfile .
$ sudo podman build -t flask36 --no-cache --build-arg PYTHON=python3 --file=OCIfile .
```

Now we can run either one of them, and rebuild them as needed without modifying the OCIfile between the two.
Go ahead and confirm with:

```bash
$ sudo podman run --rm \
    -p 5000:5000 \
    -e DBUSER="user" \
    -e DBPASS="pass" \
    -e DBHOST="workstation.example.com" \
    -e DBNAME="db" \
    flask27
```
and

```bash
$ sudo podman run --rm \
    -p 5000:5000 \
    -e DBUSER="user" \
    -e DBPASS="pass" \
    -e DBHOST="workstation.example.com" \
    -e DBNAME="db" \
    flask36
```
