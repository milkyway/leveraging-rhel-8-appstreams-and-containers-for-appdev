[//]: # (Comment: This document uses the practice of breaking all sentences in to new lines to ease diff comparisons.)

# Summit Lab: Leveraging AppStreams and Containers for App Dev

The RHEL8 Beta makes multiple versions of applications available to users to select and deploy with their applications.
How does this work?
Why would I need it?

Come to this lab to experiment with the new AppStreams yourself.
The instructors will guide you through developing an application with AppStreams.
Along the way you will learn about:

* how and when to use an AppStream
* what AppStreams are available
* AppStream profiles & defaults
* using AppStreams with containers

Come build your first application leveraging AppStreams!

# Outline

* **[Lab 0](labs/lab0/chapter0.md)**: Lab Setup & Basic Commands
* **[Lab 1](labs/lab1/chapter1.md)**: Intro to AppStreams
  * Review of how to use AppStreams
  * Review of when to use AppStreams
  * Review of relationship between AppStreams & Modules
  * Review of what AppStreams are available from Red Hat & Elsewhere
* **[Lab 2](labs/lab2/chapter2.md)**: Python Flask
  * Why you might want to modularize something
  * How to modularize Python Flask
  * How to build the module
  * How to make the module available
* **[Lab 3](labs/lab3/chapter3.md)**: Using AppStreams and Modules
  * Create application
  * Deploy backend in a container(s)
    * Review Container Catalog
    * Leverage Red Hat provided database containers
  * Deploy front-end (flask) in a container
* **[Lab 4](labs/lab4/chapter4.md)**: Migrate to OpenShift?
  * Need local deploys of:
    * containers required
    * openshift
    * git forge
  * Migrate to using git triggered builds with openshift

